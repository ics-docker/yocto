# yocto

[Docker](https://www.docker.com) image to use [yocto](https://www.yoctoproject.org).

This is a fork of https://github.com/crops/yocto-dockerfiles to keep a local image (`crops` isn't an _official_ docker organisation).

Only `registry.esss.lu.se/ics-docker/yocto:debian-9-base` is built.
The image is built for testing when pushing to a branch other than master.
When merging into master, the image is built and pushed to the gitlab registry.

The master branch is protected. MR has to be used for updates.
